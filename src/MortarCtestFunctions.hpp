/** \file MortarCtestFunctions.hpp
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */
#pragma once

#ifndef __MORTAR_CTEST_FUNCTIONS_HPP__
#define __MORTAR_CTEST_FUNCTIONS_HPP__

namespace MortarCtestFunctions {

enum contact_tests {
  EIGHT_CUBE = 1,
  T_INTERFACE = 2,
  PUNCH_TOP_AND_MID = 3,
  PUNCH_TOP_ONLY = 4,
  PUNCH_TOP_ONLY_ALM = 5,
  SMILING_FACE = 6,
  SMILING_FACE_CONVECT = 7,
  WAVE_2D = 8,
  WAVE_2D_ALM = 9,
  NIGNT_HERTZ_2D = 10,
  NIGNT_WAVE_2D = 11,
  NIGNT_HERTZ_3D = 12,
  LAST_TEST
};

MoFEMErrorCode get_night_hertz_2D_results(int ss, double &exp_energy,
                                          double &expected_contact_area,
                                          int &exp_nb_gauss_pts) {
  MoFEMFunctionBegin;
  switch (ss) {
  case 0:
    exp_energy = 1.6140e-08;
    expected_contact_area = 0.001342793;
    exp_nb_gauss_pts = 4230;
    break;
  case 1:
    exp_energy = 5.9959e-08;
    expected_contact_area = 0.001860186;
    exp_nb_gauss_pts = 5876;
    break;
  case 2:
    exp_energy = 1.2880e-07;
    expected_contact_area = 0.002319399;
    exp_nb_gauss_pts = 7356;
    break;
  case 3:
    exp_energy = 2.2131e-07;
    expected_contact_area = 0.002683898;
    exp_nb_gauss_pts = 8550;
    break;
  case 4:
    exp_energy = 3.3642e-07;
    expected_contact_area = 0.002929756;
    exp_nb_gauss_pts = 9360;
    break;
  case 5:
    exp_energy = 4.7351e-07;
    expected_contact_area = 0.003192519;
    exp_nb_gauss_pts = 10220;
    break;
  case 6:
    exp_energy = 6.3188e-07;
    expected_contact_area = 0.003467593;
    exp_nb_gauss_pts = 11110;
    break;
  case 7:
    exp_energy = 8.1095e-07;
    expected_contact_area = 0.003689629;
    exp_nb_gauss_pts = 11840;
    break;
  case 8:
    exp_energy = 1.0103e-06;
    expected_contact_area = 0.003906358;
    exp_nb_gauss_pts = 12935;
    break;
  case 9:
    exp_energy = 1.2296e-06;
    expected_contact_area = 0.004150522;
    exp_nb_gauss_pts = 13788;
    break;
  default:
    SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
            "Too many steps for Test NIGNT_HERTZ_2D");
  }

  MoFEMFunctionReturn(0);
};

MoFEMErrorCode get_night_hertz_3D_results(int ss, double &exp_energy,
                                          double &expected_contact_area,
                                          int &exp_nb_gauss_pts) {
  MoFEMFunctionBegin;
  switch (ss) {
  case 0:
    exp_energy = 2.4921e-07;
    expected_contact_area = 0.037387570;
    exp_nb_gauss_pts = 11554;
    break;
  case 1:
    exp_energy = 8.5849e-07;
    expected_contact_area = 0.053806117;
    exp_nb_gauss_pts = 14912;
    break;
  case 2:
    exp_energy = 1.7168e-06;
    expected_contact_area = 0.072368224;
    exp_nb_gauss_pts = 17395;
    break;
  case 3:
    exp_energy = 2.6893e-06;
    expected_contact_area = 0.075171507;
    exp_nb_gauss_pts = 19006;
    break;
  case 4:
    exp_energy = 3.9262e-06;
    expected_contact_area = 0.080084399;
    exp_nb_gauss_pts = 20744;
    break;
  case 5:
    exp_energy = 5.4079e-06;
    expected_contact_area = 0.084318672;
    exp_nb_gauss_pts = 22342;
    break;
  case 6:
    exp_energy = 7.1375e-06;
    expected_contact_area = 0.087477064;
    exp_nb_gauss_pts = 23951;
    break;
  case 7:
    exp_energy = 9.0959e-06;
    expected_contact_area = 0.088494524;
    exp_nb_gauss_pts = 24764;
    break;
  case 8:
    exp_energy = 1.1310e-05;
    expected_contact_area = 0.091510940;
    exp_nb_gauss_pts = 26403;
    break;
  case 9:
    exp_energy = 1.3771e-05;
    expected_contact_area = 0.095836947;
    exp_nb_gauss_pts = 27461;
    break;
  default:
    SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
            "Too many steps for Test NIGNT_HERTZ_2D");
  }

  MoFEMFunctionReturn(0);
};

MoFEMErrorCode get_night_wave_2D_results(int ss, double &exp_energy,
                                         double &expected_contact_area,
                                         int &exp_nb_gauss_pts) {
  MoFEMFunctionBegin;
  switch (ss) {
  case 0:
    exp_energy = 1.7327e-04;
    expected_contact_area = 0.015624916;
    exp_nb_gauss_pts = 768;
    break;
  case 1:
    exp_energy = 6.1650e-04;
    expected_contact_area = 0.025471946;
    exp_nb_gauss_pts = 1288;
    break;
  case 2:
    exp_energy = 1.2469e-03;
    expected_contact_area = 0.031249908;
    exp_nb_gauss_pts = 1578;
    break;
  case 3:
    exp_energy = 2.0878e-03;
    expected_contact_area = 0.031249908;
    exp_nb_gauss_pts = 1578;
    break;
  case 4:
    exp_energy = 3.1689e-03;
    expected_contact_area = 0.031249929;
    exp_nb_gauss_pts = 1600;
    break;
  case 5:
    exp_energy = 4.4035e-03;
    expected_contact_area = 0.039604827;
    exp_nb_gauss_pts = 2013;
    break;
  case 6:
    exp_energy = 5.7279e-03;
    expected_contact_area = 0.042453153;
    exp_nb_gauss_pts = 2153;
    break;
  case 7:
    exp_energy = 7.1716e-03;
    expected_contact_area = 0.046874962;
    exp_nb_gauss_pts = 2370;
    break;
  case 8:
    exp_energy = 8.7560e-03;
    expected_contact_area = 0.046874962;
    exp_nb_gauss_pts = 2370;
    break;
  case 9:
    exp_energy = 1.0527e-02;
    expected_contact_area = 0.046874962;
    exp_nb_gauss_pts = 2370;
    break;
  default:
    SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
            "Too many steps for Test NIGNT_HERTZ_2D");
  }

  MoFEMFunctionReturn(0);
};

MoFEMErrorCode check_tests(int ss, int test_num, double &expected_energy,
                           double &expected_contact_area,
                           int &expected_nb_gauss_pts) {
  MoFEMFunctionBegin;

  if (test_num) {

    switch (test_num) {
    case EIGHT_CUBE:
      expected_energy = 3.0e-04;
      expected_contact_area = 2.999995061;
      expected_nb_gauss_pts = 5718;
      break;
    case T_INTERFACE:
      expected_energy = 3.0e-04;
      expected_contact_area = 1.749997733;
      expected_nb_gauss_pts = 2046;
      break;
    case PUNCH_TOP_AND_MID:
      expected_energy = 7.8125e-05;
      expected_contact_area = 0.062499557;
      expected_nb_gauss_pts = 3408;
      break;
    case PUNCH_TOP_ONLY:
      expected_energy = 2.4110e-05;
      expected_contact_area = 0.062499557;
      expected_nb_gauss_pts = 3408;
      break;
    case PUNCH_TOP_ONLY_ALM:
      expected_energy = 2.4104e-05;
      expected_contact_area = 0.062499557;
      expected_nb_gauss_pts = 3408;
      break;
    case SMILING_FACE:
      expected_energy = 7.5944e-04;
      expected_contact_area = 2.727269391;
      expected_nb_gauss_pts = 1020;
      break;
    case SMILING_FACE_CONVECT:
      expected_energy = 7.2982e-04;
      expected_contact_area = 2.230868859;
      expected_nb_gauss_pts = 794;
      break;
    case WAVE_2D:
      expected_energy = 0.008537863;
      expected_contact_area = 0.125;
      expected_nb_gauss_pts = 384;
      break;
    case WAVE_2D_ALM:
      expected_energy = 0.008538894;
      expected_contact_area = 0.125;
      expected_nb_gauss_pts = 384;
      break;
    case NIGNT_HERTZ_2D:
      CHKERR get_night_hertz_2D_results(
          ss, expected_energy, expected_contact_area, expected_nb_gauss_pts);
      break;
    case NIGNT_WAVE_2D:
      CHKERR get_night_wave_2D_results(
          ss, expected_energy, expected_contact_area, expected_nb_gauss_pts);
      break;
    case NIGNT_HERTZ_3D:
      CHKERR get_night_hertz_3D_results(
          ss, expected_energy, expected_contact_area, expected_nb_gauss_pts);
      break;
    default:
      SETERRQ1(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
               "Unknown test number %d", test_num);
    }
  }
  MoFEMFunctionReturn(0);
}

} // namespace MortarCtestFunctions

#endif