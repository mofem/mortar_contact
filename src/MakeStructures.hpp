/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __MORTAR_CONTACT_STRUCTURES__
#define __MORTAR_CONTACT_STRUCTURES__

struct MortarContactStructures {

  static MoFEMErrorCode masterSlaveTrianglesCreation(MoFEM::Interface &m_field,
                                                     Range &range_surf_master,
                                                     Range &range_surf_slave,
                                                     int step = 1) {
    MoFEMFunctionBegin;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
      if (it->getName().compare(0, 13, "MORTAR_MASTER") == 0) {
        CHKERR m_field.get_moab().get_entities_by_type(it->meshset, MBTRI,
                                                       range_surf_master, true);
      }
    }

    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
      if (it->getName().compare(0, 12, "MORTAR_SLAVE") == 0) {
        rval = m_field.get_moab().get_entities_by_type(it->meshset, MBTRI,
                                                       range_surf_slave, true);
        CHKERRQ_MOAB(rval);
      }
    }

    MoFEMFunctionReturn(0);
  }

  static MoFEMErrorCode findContactSurfacePairs(
      MoFEM::Interface &m_field,
      std::vector<std::pair<Range, Range>> &contact_surface_pairs) {
    MoFEMFunctionBegin;

    std::map<std::string, Range> slave_surface_map, master_surface_map;

    auto fill_surface_map = [&](auto &surface_map, std::string surface_name) {
      MoFEMFunctionBegin;

      for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
        if (it->getName().compare(0, surface_name.size(), surface_name) == 0) {
          Range tris;
          CHKERR m_field.get_moab().get_entities_by_type(it->meshset, MBTRI,
                                                         tris, true);
          std::string key = it->getName().substr(surface_name.size());
          auto res =
              surface_map.insert(std::pair<std::string, Range>(key, tris));
          if (!res.second) {
            SETERRQ1(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                     "Contact surface name %s used more than once",
                     it->getName().c_str());
          }
        }
      }

      MoFEMFunctionReturn(0);
    };

    CHKERR fill_surface_map(slave_surface_map, "MORTAR_SLAVE");
    CHKERR fill_surface_map(master_surface_map, "MORTAR_MASTER");

    for (const auto &[key, slave_range] : slave_surface_map) {
      auto master_search = master_surface_map.find(key);
      if (master_search != master_surface_map.end()) {
        contact_surface_pairs.push_back(
            std::pair<Range, Range>(slave_range, master_search->second));
        master_surface_map.erase(master_search);
      } else {
        SETERRQ2(
            PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
            "Cannot find pairing contact surface MORTAR_MASTER%s for existing "
            "surface MORTAR_SLAVE%s",
            key.c_str(), key.c_str());
      }
    }

    if (!master_surface_map.empty()) {
      std::string key = master_surface_map.begin()->first;
      SETERRQ2(
          PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
          "Cannot find pairing contact surface MORTAR_SLAVE%s for existing "
          "surface MORTAR_MASTER%s",
          key.c_str(), key.c_str());
    }

    MoFEMFunctionReturn(0);
  }
};
#endif