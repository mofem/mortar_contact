/** \file Mortar.hpp
  \ingroup Header file for basic finite elements implementation
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __MORTAR_HPP__
#define __MORTAR_HPP__

#include <BasicFiniteElements.hpp>

#include <SimpleContact.hpp>
#include <clipper.hpp>
#include <moab/AdaptiveKDTree.hpp>

using namespace ClipperLib;
#include <ContactSearchKdTree.hpp>
#include <MortarContactProblem.hpp>
#include <MakeStructures.hpp>
#include <MortarContactInterface.hpp>

#endif // __MORTAR_HPP__