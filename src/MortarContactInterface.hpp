/** \file MortarContactInterface.hpp
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */
#pragma once

#ifndef __MORTARCONTACTINTERFACE_HPP__
#define __MORTARCONTACTINTERFACE_HPP__

struct MortarContactInterface : public GenericElementInterface {

  MoFEM::Interface &mField;
  SmartPetscObj<DM> dM;

  // params
  double cnValue;
  PetscInt oRder;
  PetscInt orderContact;
  PetscInt orderLambda;
  PetscInt nbHoLevels;
  PetscBool isNewtonCotes;
  PetscBool convectPts;
  PetscBool printContactState;
  PetscBool almFlag;
  PetscBool dEbug;
  PetscBool isPartitioned;

  // data structs
  boost::shared_ptr<MortarContactProblem> contactProblemPtr;
  Range contactPrisms;
  Range masterTris, slaveTris;
  std::vector<std::pair<Range, Range>> contactSurfacePairs; // <Slave, Master>
  
  Range postProcSurface;
  double postProcGapTol;

  string positionField;
  string meshNodeField;

  bool isDisplacementField;
  bool isQuasiStatic;

  std::vector<BitRefLevel> bitLevels;

  typedef ContactSearchKdTree::ContactCommonData_multiIndex contactMIndex;
  boost::shared_ptr<contactMIndex> contactCommondataMultiIndex;
  boost::shared_ptr<PostProcFaceOnRefinedMesh> postProcContactPtr;

  boost::shared_ptr<MortarContactProblem::MortarContactElement>
      fePostProcSimpleContact;
  boost::shared_ptr<MortarContactProblem::CommonDataMortarContact>
      commonDataPostProc;

  moab::Core mb_post;
  moab::Interface &moabPostProcMesh;
  std::array<double, 2> nbGaussPts;
  std::array<double, 2> contactArea;

  MortarContactInterface(MoFEM::Interface &m_field, string postion_field,
                         string mesh_posi_field_name = "MESH_NODE_POSITIONS",
                         bool is_displacement_field = false,
                         bool is_quasi_static = true)
      : mField(m_field), positionField(postion_field),
        meshNodeField(mesh_posi_field_name),
        isDisplacementField(is_displacement_field),
        isQuasiStatic(is_quasi_static), moabPostProcMesh(mb_post) {

    oRder = -1;
    orderContact = 1;
    nbHoLevels = 0;
    orderLambda = 1;
    cnValue = -1;
    postProcGapTol = 0.;
    almFlag = PETSC_FALSE;
    isNewtonCotes = PETSC_FALSE;
    convectPts = PETSC_FALSE;
    printContactState = PETSC_FALSE;
    dEbug = PETSC_FALSE;
    isPartitioned = PETSC_FALSE;
  }
  MortarContactInterface() = delete;

  // interface functions
  MoFEMErrorCode getCommandLineParameters() override;
  MoFEMErrorCode addElementFields() override;

  MoFEMErrorCode createElements() override;
  MoFEMErrorCode setOperators() override;
  BitRefLevel getBitRefLevel() override;
  MoFEMErrorCode addElementsToDM(SmartPetscObj<DM> dm) override;

  // MoFEMErrorCode setupSolverKSP() override;
  MoFEMErrorCode setupSolverJacobianSNES() override;
  MoFEMErrorCode setupSolverFunctionSNES() override;
  MoFEMErrorCode setupSolverJacobianTS(const TSType type) override;
  MoFEMErrorCode setupSolverFunctionTS(const TSType type) override;

  MoFEMErrorCode updateElementVariables() override;
  MoFEMErrorCode postProcessElement(int step) override;

  // contact interface
  std::vector<BitRefLevel> &getBitRefLevelVector();
  MoFEMErrorCode setupElementEntities();
  MoFEMErrorCode setPositionFieldOrder(int order);

  boost::ptr_deque<MoFEM::ForcesAndSourcesCore::UserDataOperator> &
  getPostProcSimpleContactPipeline();

  MoFEMErrorCode findContactSurfacePairs();
  MoFEMErrorCode findPostProcSurface();

  template <typename T, bool RHS>
  MoFEMErrorCode setupSolverImpl(const TSType type = IM);
  template <typename T>
  MoFEMErrorCode setupSolverFunction(const TSType type = IM);
  template <typename T>
  MoFEMErrorCode setupSolverJacobian(const TSType type = IM);

  MoFEMErrorCode postProcessContactState(int step);
};

template <>
MoFEMErrorCode
MortarContactInterface::setupSolverJacobian<SNES>(const TSType type);
template <>
MoFEMErrorCode
MortarContactInterface::setupSolverJacobian<SNES>(const TSType type);
template <>
MoFEMErrorCode
MortarContactInterface::setupSolverFunction<TS>(const TSType type);
template <>
MoFEMErrorCode
MortarContactInterface::setupSolverFunction<TS>(const TSType type);

#endif // __MORTARCONTACTINTERFACE_HPP__