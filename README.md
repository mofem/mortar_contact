[TOC]
## Brief description 
Contact interaction between elastic solids having non-matching meshes in the contact interface.

***_Assumptions:_*** small rotations, small strains, small relative tangential displacements (moderate if `-convect` option is switched on, see [Contact parameters](#markdown-header-contact-parameters) for more details).

## 1. Definition of the contact surfaces

Pairs of surfaces belonging to two solids that can potentially come into contact have to be marked as _BLOCKSET_s, with one surface belonging to the pair with name `MORTAR_MASTER` and the other surface of the pair named `MORTAR_SLAVE`. An arbitrary number of pairs of contact surfaces can be defined by adding each side either in `MORTAR_MASTER` or in `MORTAR_SLAVE` _BLOCKSET_.

### Geometry

- Create the contacting solids separately 
- Create all necessary groups for the partitioned solids:
    - Groups of volumes for solid blocks
    - Groups of faces for fixed BCs
    - Group of faces for pressure BCs
    - Group of faces for springs 
    - Group all one side of the contact pairs in the slave faces in the slave blockset and the rest of their counterparts to the master blockset.

***_NOTE:_*** Springs can be used to eliminate the rigid body motion in a contact simulation  under the pressure control. 

### Mesh

- Mesh each solid separately

## 2. Preparation of the param file 

Check the essential contact parameters in the file `param_file.petsc`: 
```bash
-order 2         
-order_lambda 1          
-cn_value 1.e3               
```

***_NOTE:_*** `cn` is the contact augmentation parameter which affects the convergence and has minimal effect on the solution (in a reasonable range of values). Recommended initial value is the Young's modulus of contacting solids (or harmonic mean in case of different values). The optimal value can be found by repetitively increasing/decreasing the initial value by e.g. a factor of 10.

### Contact parameters

Name | Description | Default value
--- | --- | ---
`-order` | Approximation order of the field of spatial positions for the entire mesh | `1`
`-order_lambda` | Approximation order of the field of contact Lagrange multipliers | `1`
`order_contact` | Approximation order of the field of spatial positions for the contact elements and a given number of tetrahedral element layers adjacent to the contact interface | `1`
`-ho_levels_num` | Number of tetrahedral element layers adjacent to the contact interface with higher order of the field of spatial positions (if `-order_contact` is higher than `order`) | `1` 
`-cn_value` | Augmentation parameter which affects the convergence and has minimal effect on the solution. Recommended initial value is the Young's modulus of contacting solids (or harmonic mean in case of different values). The optimal value can be found by repetitively increasing/decreasing the initial value by e.g. a factor of 10 | `1`
`-r_value` | Contact regularisation parameter which can lie between 1.0 and 1.1. Values greater than 1 can speed-up the convergence, but will also alter the stiffness of the contact interface, therefore it is not recommended to change this parameter | `1`
`-step_num` | Number of steps used to achieve the specified load value (so-called load control). Note that multi-stepping can be particularly important to obtain a solution for highly nonlinear problems | `1` 
`-alm_flag` | Defines the choice of the algorithm: `0` (False) - Complementarity function approach, `1` (True) - Augmented Lagrangian method | `0`  
`-convect` | If set to `1` (True), moderate relative tangential displacements can be  taken into account | `0`  


## 2. Running the contact simulation

```bash
mpirun -np 2 $HOME/mofem_install/um/build_release/mortar_contact/mortar_contact -file_name $file_name
```

## 3. Postprocessing

First, the output `h5m` files need to be converted to `vtk` format. Note that these files can be converted one by one using the `mbconvert` tool:
```bash
mbconvert out.h5m out.vtk
mbconvert out_contact.h5m out_contact.vtk
mbconvert out_mortar_contact_integ_pts.h5m out_mortar_contact_integ_pts.vtk
```
or all at once by using the multiprocessing script `convert.py`:
```bash
$HOME/mofem_install/um/build_release/tools/convert.py -np 2 out*
```
The obtained `vtk` files can be viewed in *Paraview*, in particular:

- File `out.vtk` contains the stress tensor components (tag `SPATIAL_POSITION_PIOLA1_STRESS`), as well as material coordinates (tag `MESH_NODE_POSITIONS`) and current coordinates (tag `SPATIAL_POSITION`), which can be used to compute the displacement field with the *Calculator* filter as `DISPLACEMENT=SPATIAL_POSITION-MESH_NODE_POSITIONS`
- File `out_contact.vtk` contains the nodal interpolation of the Lagrange multipliers equivalent to the contact pressure (tag `LAGMULT`)
- File `out_mortar_contact_integ_pts.vtk` contains values of Lagrange multipliers (tag `LAGMULT`) and the normal gap (tag `GAP`) at Gauss points of the contact interface. Note that the _Point Gaussian_ representation or alternatively the _Glyph_ filter should be used for their visualisation in *Paraview*.

## 4. Simulation with internal stress
Simulation taking into account non-matching-meshes contact and internal stress is  possible using executable `mortar_contact_thermal`. Furthermore, this program provides possibility to save the value of the internal stress and the corresponding actual stress on the input mesh for subsequent use in the fracture module.

Two options are available for the internal stress:

- internal stress caused by thermal expansion given by an analytical formula; by default the temperature is decreased by 1 degree compared to the initial one, however, this can be changed to a different formula, e.g. coordinate-dependant in the function `thermal_strain` in file `mortar_contact_thermal.cpp`
- internal stress pre-computed such that its integration points values are saved on tags of the input mesh

Two options are available for saving the internal and corresponding actual stress on the mesh:

- mean values of internal and actual stress saved on tags `MED_INTERNAL_STRESS` and `MED_ACTUAL_STRESS`, respectively 
- values of internal and actual stress at integration points saved using tags `INTERNAL_STRESS` and `ACTUAL_STRESS`, respectively

In addition to outlined above parameters, the following parameters can be used with `mortar_contact_thermal`:
Name | Description | Default value
--- | --- | ---
`-analytical_input` | If set to `1` (True), compute internal stress using analytical formula for thermal strain, otherwise use pre-computed internal stress saved on tags of the input mesh | `1`
`-thermal_expansion_coef` | Value of thermal expansion coefficient for computing thermal strain (used if `-analytical_input` is set to `1`) | `1e-5`
`-init_temp` | Value of initial temperature for computing thermal strain (used if `-analytical_input` is set to `1`) | `250.0`
`-stress_tag_name` | Tag name used when reading internal stress from the mesh (if `-analytical_input` is set to `0`) | `INTERNAL_STRESS`
`-save_mean_stress` |  If set to `1` (True), save mean values of internal and actual stress on the tags of the mesh, otherwise save integration point values of stresses | `1`
`-scale_factor` | Scale for the internal stress | `1.0`
`-ignore_contact` | If set to `1` (True), ignore contact | `0` 
`-ignore_pressure` | If set to `1` (True), ignore pressure | `0` 
`-output_mesh_file` | Name of the file for writing the mesh with internal and actual stress saved on tags | `0` 

Example of running the simulation using the analytical thermal strain for the internal stress and saving the mean values of the internal and external stress on the mesh:
```bash
mpirun -np 4 simple_contact_thermal \
-file_name test.cub \
-output_mesh_file med_stress.h5m \
-analytical_input 1
-thermal_expansion_coef 1e-3 \
-init_temp 250. \
-save_mean_stress 1 \
-scale_factor 1 \
```


