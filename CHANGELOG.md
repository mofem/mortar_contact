### Mortar Contact Module v0.10.0

- This is the initial release of the module
- Generation of mortar contact prisms 
- Clipping for common area polygons of mortar contact elements
- Gauss points location and weights generation
- ctests and night tests handling